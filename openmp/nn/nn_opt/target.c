#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "ti_omp_device.h"
#define REC_LENGTH 49	// size of a record in db
#define LATITUDE_POS 28	// location of latitude coordinates in input record
#pragma omp end declare target

typedef struct neighbor {
	char entry[REC_LENGTH];
	double dist;
} neighbor;


void heap_init_msmc(char *p, size_t bytes) 
{ 
#pragma omp target map(to: bytes, p[0:bytes])
   {
      __heap_init_msmc(p,bytes); 
   }
}


neighbor* MainLoop(int threads, char *sandbox, int k, int dbs, int target_lat, int target_long, neighbor *neighbors)
{
	int len = strlen(sandbox);
	int size = len/REC_LENGTH;
	printf("total entries:%d\n\n",size);	

	#pragma omp target map(to: threads, len, sandbox[0:len], size, k, dbs, target_lat, target_long) map(tofrom: neighbors[0:k])
	{
		/* Launch threads to  */
		int i, j;
		char *rec_iter;
		float tmp_lat = 0.0, tmp_long = 0.0;
		float *z;
		z  = (float *) __malloc_msmc(size * sizeof(float));
		double time0, time1;
		time0 = omp_get_wtime();
		double stopwatch;
		double duration = 0;

		stopwatch = omp_get_wtime();

		#pragma omp parallel for shared(z, target_lat, target_long) private(i, rec_iter, tmp_lat, tmp_long) num_threads(threads)
		for( i = 0 ; i < size; i++ ) {
			rec_iter = sandbox+(i * REC_LENGTH + LATITUDE_POS - 1);
			sscanf(rec_iter, "%f %f", &tmp_lat, &tmp_long);
			z[i] = sqrt(( (tmp_lat-target_lat) * (tmp_lat-target_lat) )+( (tmp_long-target_long) * (tmp_long-target_long) ));
		} /* omp end parallel */
		#pragma omp barrier
		duration = duration + omp_get_wtime() - stopwatch;//Du

		for( i = 0 ; i < size; i++ ) {
			float max_dist = -1;
			int max_idx = 0;
			// find a neighbor with greatest dist and take his spot if allowed!
			for( j = 0 ; j < k ; j++ ) {
				if( neighbors[j].dist > max_dist ) {
					max_dist = neighbors[j].dist;
					max_idx = j;
				}
			}
			// compare each record with max value to find the nearest neighbor
			if( z[i] < neighbors[max_idx].dist ) {
				sandbox[(i+1)*REC_LENGTH-1] = '\0';
			  	strcpy(neighbors[max_idx].entry, sandbox +i*REC_LENGTH);
			  	neighbors[max_idx].dist = z[i];
			}
		}

		time1 = omp_get_wtime();		
		printf("\nduration in DSP: %f s.\n\n", time1-time0);
		printf("PR Time: %f s.\n",duration);
	}//end of offload
	return neighbors;
}



void runTest( int omp_num_threads) 
{	
	#pragma omp target map(to: omp_num_threads)
	{
		printf("Here: using %d out of 8 threads. \n", omp_num_threads);

	}
}
