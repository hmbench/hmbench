#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
extern int maximum( int a, int b, int c);
//#define TRACEBACK
#pragma omp end declare target

	int blosum62[24][24] = {
{ 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4},
{-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4},
{-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4},
{-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4},
{ 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4},
{-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4},
{-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
{ 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4},
{-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4},
{-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4},
{-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4},
{-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4},
{-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4},
{-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4},
{-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4},
{ 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4},
{ 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4},
{-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4},
{-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4},
{ 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4},
{-2, -1,  3,  4, -3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4},
{-1,  0,  0,  1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
{ 0, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4},
{-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1}
};


int maximum( int a, int b, int c)
	{
	int k;
	if( a <= b )
		k = b;
	else
		k = a;

	if( k <=c )
		return(c);
	else
		return(k);
}


////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void runTest( int iterations, int max_rows, int max_cols, int penalty, int omp_num_threads) 
{	
	#pragma omp target map(to:iterations, max_rows, max_cols, penalty, omp_num_threads)
	{
//		printf("Here3: max_rows = %d\n",max_cols);
		double time0, time1;
		time0 = omp_get_wtime();

		int idx, index;
		int *input_itemsets, *output_itemsets, *referrence;
		int i=0,j=0, repeat;
		double stopwatch;//Du
		double duration = 0;//Du

		max_rows = max_rows + 1;
		max_cols = max_cols + 1;
		referrence = (int *)malloc( max_rows * max_cols * sizeof(int) );
		input_itemsets = (int *)malloc( max_rows * max_cols * sizeof(int) );
		output_itemsets = (int *)malloc( max_rows * max_cols * sizeof(int) );
		
		if (!input_itemsets)
			fprintf(stderr, "error: can not allocate memory:input_itemsets");
		if (!referrence)
			fprintf(stderr, "error: can not allocate memory:referrence");
		if (!output_itemsets)
			fprintf(stderr, "error: can not allocate memory:output_itemsets");

		srand ( 7 );
		
//		printf("Here5:cols = %d, rows=%d, sizeof int= %d\n",max_cols,max_rows,sizeof(int));
		for (i = 0 ; i < max_cols; i++){
			for ( j = 0 ; j < max_rows; j++){
				input_itemsets[i*max_cols+j] = 0;
			}
		}

		printf("Start Needleman-Wunsch\n");

///////
		for (repeat=0; repeat < iterations ; repeat++ )
		{
			for( i=1; i< max_rows ; i++){    //please define your own sequence. 
			   input_itemsets[i*max_cols] = rand() % 10 + 1;
			}
			for( j=1; j< max_cols ; j++){    //please define your own sequence.
			   input_itemsets[j] = rand() % 10 + 1;
			}
			
			//printf("Here6\n");
			for ( i = 1 ; i < max_cols; i++){
				for ( j = 1 ; j < max_rows; j++){
				referrence[i*max_cols+j] = blosum62[input_itemsets[i*max_cols]][input_itemsets[j]];
				}
			}
			//printf("Here7\n");

			for( i = 1; i< max_rows ; i++)
			   input_itemsets[i*max_cols] = -i * penalty;
			for( j = 1; j< max_cols ; j++)
			   input_itemsets[j] = -j * penalty;
			
			//Compute top-left matrix 
			
			for( i = 0 ; i < max_cols-2 ; i++){
				stopwatch = omp_get_wtime();//Du
				#pragma omp parallel for shared(input_itemsets) firstprivate(i,max_cols,penalty) private(idx, index) num_threads(omp_num_threads)
				for( idx = 0 ; idx <= i ; idx++){
					index = (idx + 1) * max_cols + (i + 1 - idx);
					input_itemsets[index]= maximum( input_itemsets[index-1-max_cols]+ referrence[index], 
												 input_itemsets[index-1]         - penalty, 
												 input_itemsets[index-max_cols]  - penalty);
				}
				duration = duration + omp_get_wtime() - stopwatch;//Du
			}
			
			//Compute bottom-right matrix 
			for( i = max_cols - 4 ; i >= 0 ; i--){
			//omp_set_num_threads(omp_num_threads);//Du
			stopwatch = omp_get_wtime();//Du
			#pragma omp parallel for shared(input_itemsets) firstprivate(i,max_cols,penalty) private(idx, index) num_threads(omp_num_threads)
			   for( idx = 0 ; idx <= i ; idx++){
				  index =  ( max_cols - idx - 2 ) * max_cols + idx + max_cols - i - 2 ;
				  input_itemsets[index]= maximum( input_itemsets[index-1-max_cols]+ referrence[index], 
												  input_itemsets[index-1]         - penalty, 
												  input_itemsets[index-max_cols]  - penalty);
				  }
			duration = duration + omp_get_wtime() - stopwatch;//Du
			}
		}

		free(referrence);
		free(input_itemsets);
		free(output_itemsets);
		
		time1 = omp_get_wtime();
		printf("Duration in device: %f s.\n",time1-time0);
		printf("PR Time: %f s.\n",duration);//Du
	}
}
