/*

offload target file

*/
#pragma omp declare target
#include <stdio.h>
#include <omp.h>
extern long long get_time(void);
#pragma omp end declare target

long long get_time(void){
	long long ts;
	ts = (long long) (1000000*omp_get_wtime());
	return ts;
}


float* lud_omp(float *a, int size, int omp_num_threads)
{
	#pragma omp target map(to:size, omp_num_threads) map(tofrom: a[0:size])
	{
		long long time0;
		long long time1;
		time0 = get_time();

		 int i,j,k;
		 float sum;
		 printf("num of threads = %d\n", omp_num_threads);
		 for (i=0; i <size; i++){
		//	omp_set_num_threads(omp_num_threads);//Du
	#pragma omp parallel for default(none) \
			 private(j,k,sum) shared(size,i,a) num_threads(omp_num_threads)
			 for (j=i; j <size; j++){
				 sum=a[i*size+j];
				 for (k=0; k<i; k++) sum -= a[i*size+k]*a[k*size+j];
				 a[i*size+j]=sum;
			 }
	#pragma omp parallel for default(none) \
			 private(j,k,sum) shared(size,i,a) num_threads(omp_num_threads)
			 for (j=i+1;j<size; j++){
				 sum=a[j*size+i];
				 for (k=0; k<i; k++) sum -=a[j*size+k]*a[k*size+i];
				 a[j*size+i]=sum/a[i*size+i];
			 }
		 }
	
	time1 = get_time();
	printf("Time consumed offload: %lf ms \n", (float) (time1-time0) / 1000);	
	}
	return a;
}
