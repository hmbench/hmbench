#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
extern void copy (float* dst, float* src, int N, int Threads);
extern void compute_step_factor (int nelr, float* variables, float* areas, float* step_factors, int num_th);
#define MIN(a, b) ((a)<=(b) ? (a) : (b))
#pragma omp end declare target


int* MainLoop(int iterations, int threads, int cols, int rows, int** wall, int* result)
{
	int i,j;
	int* wall_arr = (int*) malloc(sizeof(int)*rows*cols);
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			wall_arr[i*cols+j] = wall[i][j];
		}
	}
	
	int* destination = (int*) malloc(sizeof(int)*cols);

	#pragma omp target map(to: iterations, threads, cols, rows, wall_arr[0:rows*cols], result[0:cols]) map(tofrom:destination[0:cols])
	{	
		double time0, time1;
		time0 = omp_get_wtime();
		int *src, *temp;
		int min;
		int i, j, repeat;
		double stopwatch;//Du
		double duration = 0;//Du

		src = (int*) malloc(sizeof(int) * cols);	// src = new int[cols];

		int **wall_mat = (int**)malloc(rows*sizeof(int*));
		for (i=0; i<rows; i++)
			wall_mat[i] = wall_arr + cols*i;
		
		for (repeat=0; repeat < iterations ; repeat++ )
		{
			for (i=0; i < cols ; i++)
			{
				destination[i] = result[i];
				src[i] = 0;
			}//Du: pure repeating, not converging on anything

			for (i = 0; i < rows-1; i++) {
				temp = src;
				src = destination;
				destination = temp;

				stopwatch = omp_get_wtime();//Du 
				#pragma omp parallel for private(min) num_threads(threads)
				for( j = 0; j < cols; j++){
						min = src[j];
					if (j > 0)
						min = MIN(min, src[j-1]);
					if (j < cols-1)
						min = MIN(min, src[j+1]);
					destination[j] = wall_mat[i+1][j] + min;				 
				}
				duration = duration + omp_get_wtime() - stopwatch;//Du 
			}
		}


	time1 = omp_get_wtime();
	printf("\nDuration in DSP: %f s.\n", time1-time0);
	printf("PR Time: %f s.\n",duration);//Du

	}//end of offload
	return destination;
}



void runTest( int iterations)
{	
	#pragma omp target map(to: iterations)
	{	
		printf("Hello from DSP, using %d out of 8 cores.\n",iterations);
	}
}
