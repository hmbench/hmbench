#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "ti_omp_device.h"
typedef struct float3 { float x, y, z; } float3;
extern void copy (float* dst, float* src, int N, int Threads);
#pragma omp end declare target

#include <stdbool.h> //bool, true, or false

typedef struct Node
{
	int starting;
	int no_of_edges;
} Node;


void heap_init_msmc(char *p, size_t bytes) 
{ 
#pragma omp target map(to: bytes, p[0:bytes])
   {
      __heap_init_msmc(p,bytes); 
   }
}


int* LoopHere(int iterations, int num_omp_threads, int no_of_nodes, int edge_list_size, Node* h_graph_nodes, int* h_cost, int* h_graph_edges)
{
	printf("Start traversing the tree...\n");
	
	//int k=0;
	#pragma omp target map(to: num_omp_threads, no_of_nodes, edge_list_size, h_graph_nodes[0:no_of_nodes], h_graph_edges[0:edge_list_size]) map(tofrom:h_cost[0:no_of_nodes])
	{
		int i, k=0, source=0;
		bool stop;
		double stopwatch, duration = 0;

		Node* h_graph_nodes_msmc = (Node*) __malloc_msmc(sizeof(Node)*no_of_nodes);
		int* h_graph_edges_msmc = (int*) __malloc_msmc(sizeof(int)*edge_list_size);
		
		memcpy(h_graph_nodes_msmc, h_graph_nodes, sizeof(Node)*no_of_nodes );
		memcpy(h_graph_edges_msmc, h_graph_edges, sizeof(int)*edge_list_size );
		
		bool *h_graph_mask = (bool*) __malloc_msmc(sizeof(bool)*no_of_nodes);
		bool *h_updating_graph_mask = (bool*) __malloc_msmc(sizeof(bool)*no_of_nodes);
		bool *h_graph_visited = (bool*) __malloc_msmc(sizeof(bool)*no_of_nodes);
		int* h_cost_msmc = (int*) __malloc_msmc(sizeof(int)*no_of_nodes);

		while (k<iterations)
		{	
			for( i = 0; i < no_of_nodes; i++) 
			{				
				h_graph_mask[i]=false;
				h_updating_graph_mask[i]=false;
				h_graph_visited[i]=false;
				h_cost_msmc[i]=-1;
			}

			h_graph_mask[source]=true;
			h_graph_visited[source]=true;
			h_cost_msmc[source]=0;

			do
			{
				stop=false;
				int tid;
				stopwatch = omp_get_wtime();

				#pragma omp parallel for num_threads(num_omp_threads)
				for(tid = 0; tid < no_of_nodes; tid++ )
				{
					if (h_graph_mask[tid] == true){ 
					h_graph_mask[tid]=false;
					int i;
					for(i=h_graph_nodes[tid].starting; i<(h_graph_nodes[tid].no_of_edges + h_graph_nodes[tid].starting); i++)
						{
						int id = h_graph_edges[i];
						if(!h_graph_visited[id])
							{
							h_cost_msmc[id]=h_cost_msmc[tid]+1;
							h_updating_graph_mask[id]=true;
							}
						}
					}
				}
				duration = duration + omp_get_wtime() - stopwatch;

				for(tid=0; tid< no_of_nodes ; tid++ )
				{
					if (h_updating_graph_mask[tid] == true){
					h_graph_mask[tid]=true;
					h_graph_visited[tid]=true;
					stop=true;
					h_updating_graph_mask[tid]=false;
					}
				}
			}
			while(stop);
			k++;
		}

		for( i = 0; i < no_of_nodes; i++) 
			h_cost[i] = h_cost_msmc[i];//copy out the results
		
		__free_msmc(h_graph_nodes_msmc);
		__free_msmc(h_graph_mask);
		__free_msmc(h_updating_graph_mask);
		__free_msmc(h_graph_visited);
		__free_msmc(h_graph_edges_msmc);
		__free_msmc(h_cost_msmc);
		printf("PR Time = %f s.\n", duration);

	}//end of target

	return h_cost;
}

void runTest( int iterations)
{	
	#pragma omp target map(to: iterations)
	{	
		printf("hello from DSP\n");
	}
}
