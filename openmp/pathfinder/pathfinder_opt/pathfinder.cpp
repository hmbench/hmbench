#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <assert.h>
#include "ti_omp_device.h"

extern "C"{
	extern int* MainLoop(int iterations, int threads, int cols, int rows, int** wall, int* result);
	extern void heap_init_msmc(char*, size_t);
};

void run(int argc, char** argv);

/* define timer macros */ //modified by Du
#define pin_stats_reset()   (omp_get_wtime())
#define pin_stats_pause(cycles)   (omp_get_wtime()-cycles)
#define pin_stats_dump(cycles)    printf("Total Duration: %f s.\n", cycles)

//#define BENCH_PRINT

int rows, cols, threads, iterations;
int* data;
int** wall;
int* result;
#define M_SEED 9

void
init(int argc, char** argv)
{
	if(argc==5){
		cols = atoi(argv[1]);
		rows = atoi(argv[2]);
		threads = atoi(argv[3]);
		iterations = atoi(argv[4]);
	}else{
                printf("Usage: pathfiner width num_of_steps\n");
                exit(0);
        }
	data = new int[rows*cols];
	wall = new int*[rows];
	for(int n=0; n<rows; n++)
		wall[n]=data+cols*n;
	result = new int[cols];
	
	int seed = M_SEED;
	srand(seed);

	for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            wall[i][j] = rand() % 10;
        }
    }
    for (int j = 0; j < cols; j++)
        result[j] = wall[0][j];
#ifdef BENCH_PRINT
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d ",wall[i][j]) ;
        }
        printf("\n") ;
    }
#endif

}

void 
fatal(char *s)
{
	fprintf(stderr, "error: %s\n", s);

}

#define IN_RANGE(x, min, max)   ((x)>=(min) && (x)<=(max))
#define CLAMP_RANGE(x, min, max) x = (x<(min)) ? min : ((x>(max)) ? max : x )

int main(int argc, char** argv)
{
    run(argc,argv);
	//runTest(4);

    return EXIT_SUCCESS;
}

void run(int argc, char** argv)
{
	double time0 = omp_get_wtime();

	init(argc, argv);
	printf("\nLauching...\nRows*Cols = %d*%d\nthreads = %d\niterations = %d\n", rows, cols, threads, iterations);
	
	int HeapSize = 9 << 19;//Du
	char* HeapMSMC = (char*) __malloc_msmc(HeapSize);//Du
	heap_init_msmc(HeapMSMC, HeapSize);//Du

    double cycles;
    cycles = pin_stats_reset();

	int *dst;
	dst = new int[cols];

	double time1 = omp_get_wtime();
	dst = MainLoop(iterations, threads, cols, rows, wall, result);
	double time2 = omp_get_wtime();


	printf("dst[]=\n") ;
	for (int i = 0; i < cols; i++)
		printf("%d ",dst[i]) ;
    printf("\n") ;
	
	double time3 = omp_get_wtime();
	printf("Main Loop: %f s.\n", time2-time1);
	printf("Total Time: %f s.\n", time3-time0);

    delete [] data;
    delete [] wall;
    delete [] dst;
}

