//====================================================================================================100
//		UPDATE
//====================================================================================================100

//    2006.03   Rob Janiczek
//        --creation of prototype version
//    2006.03   Drew Gilliam
//        --rewriting of prototype version into current version
//        --got rid of multiple function calls, all code in a  
//         single function (for speed)
//        --code cleanup & commenting
//        --code optimization efforts   
//    2006.04   Drew Gilliam
//        --added diffusion coefficent saturation on [0,1]
//		2009.12 Lukasz G. Szafaryn
//		-- reading from image, command line inputs
//		2010.01 Lukasz G. Szafaryn
//		--comments

//====================================================================================================100
//	DEFINE / INCLUDE
//====================================================================================================100

#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#include "ti_omp_device.h"
#include "define.c"
#include "graphics.c"
//#include "resize.c"
//#include "timer.c"

//====================================================================================================100
//====================================================================================================100
//	MAIN FUNCTION
//====================================================================================================100
//====================================================================================================100


extern fp* offload(int niter, fp lambda, long Nr, long Nc, 
					int threads, 
					long image_ori_elem, 
					fp* image_ori,
					fp* image);
extern void heap_init_msmc(char *, size_t ) ;



int main(int argc, char *argv []){

	int HeapSize = 4864 *  (1 << 10); // 4.75MB
	char* HeapMSMC = (char*) __malloc_msmc(HeapSize);//Du
	heap_init_msmc(HeapMSMC, HeapSize);//Du
	
	// algorithm parameters
    int niter;			// nbr of iterations
    fp lambda;			// update step size
	fp* image_ori;
	fp* Image;

	long Nr,Nc;			// IMAGE nbr of rows/cols/elements
	int threads;
	long image_ori_elem;

	double time0, time1, time2, time3;
	time0 = omp_get_wtime();

	//================================================================================80
	// 	GET INPUT PARAMETERS
	//================================================================================80

	if(argc != 6){
		printf("ERROR: wrong number of arguments\n");
		return 0;
	}
	else{
		niter = atoi(argv[1]);
		lambda = atof(argv[2]);
		Nr = atoi(argv[3]);						// it is 502 in the original image
		Nc = atoi(argv[4]);						// it is 458 in the original image
		threads = atoi(argv[5]);
		printf("\nniter = %d, lambda = %f, Nr = %ld, Nc = %ld, threads = %d\n",niter,(float)lambda,Nr,Nc,threads);
	}
	

	//================================================================================80
	// 	READ IMAGE (SIZE OF IMAGE HAS TO BE KNOWN)
	//================================================================================80

	image_ori_elem = Nr*Nc;
	image_ori = (fp*)malloc(sizeof(fp) * image_ori_elem);
	Image = (fp*)malloc(sizeof(fp) * image_ori_elem);
	
	printf("heap init...done.\n");
	
	int kk = 0;
	for (kk=0; kk<image_ori_elem; kk++ )
	{
		image_ori[kk] = 2.9;
	}

	read_graphics(	"../../../data/srad/image.pgm",
								image_ori,
								Nr,
								Nc,
								1);
	time2 = omp_get_wtime();
	Image = offload(niter, lambda, Nr, Nc, threads,image_ori_elem,image_ori,Image);
	time3 = omp_get_wtime();

	write_graphics(	"image_out.pgm",
								Image,
								Nr,
								Nc,
								1,
								255);

	time1 = omp_get_wtime();
	printf("Main Loop: %f s.\n", time3-time2);
	printf("Total time: %f s.\n", time1-time0);


}


