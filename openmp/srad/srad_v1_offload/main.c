//====================================================================================================100
//		UPDATE
//====================================================================================================100

//    2006.03   Rob Janiczek
//        --creation of prototype version
//    2006.03   Drew Gilliam
//        --rewriting of prototype version into current version
//        --got rid of multiple function calls, all code in a  
//         single function (for speed)
//        --code cleanup & commenting
//        --code optimization efforts   
//    2006.04   Drew Gilliam
//        --added diffusion coefficent saturation on [0,1]
//		2009.12 Lukasz G. Szafaryn
//		-- reading from image, command line inputs
//		2010.01 Lukasz G. Szafaryn
//		--comments

//====================================================================================================100
//	DEFINE / INCLUDE
//====================================================================================================100

#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <omp.h>

#include "define.c"
#include "graphics.c"
//#include "resize.c"
//#include "timer.c"

//====================================================================================================100
//====================================================================================================100
//	MAIN FUNCTION
//====================================================================================================100
//====================================================================================================100


extern fp* offload(int niter, fp lambda, long Nr, long Nc, 
					int threads, 
					long image_ori_elem, 
					fp* image_ori,
					fp* image);


int main(int argc, char *argv []){
	
	// algorithm parameters
    int niter;			// nbr of iterations
    fp lambda;			// update step size
	fp* image_ori;
	fp* image;

	long Nr,Nc;			// IMAGE nbr of rows/cols/elements
	int threads;
	long image_ori_elem;
	
	double time0, time1, time2, time3;
	time0 = omp_get_wtime();
	//================================================================================80
	// 	GET INPUT PARAMETERS
	//================================================================================80

	if(argc != 6){
		printf("ERROR: wrong number of arguments\n");
		return 0;
	}
	else{
		niter = atoi(argv[1]);
		lambda = atof(argv[2]);
		Nr = atoi(argv[3]);						// it is 502 in the original image
		Nc = atoi(argv[4]);						// it is 458 in the original image
		threads = atoi(argv[5]);
		printf("\nniter = %d, lambda = %f, Nr = %ld, Nc = %ld, threads = %d\n",niter,(float)lambda,Nr,Nc,threads);
	}


	//================================================================================80
	// 	READ IMAGE (SIZE OF IMAGE HAS TO BE KNOWN)
	//================================================================================80

    // read image
	//image_ori_rows = 502;
	//image_ori_cols = 458;
	//image_ori_elem = image_ori_rows * image_ori_cols;
	
	image_ori_elem = Nr*Nc;
	image_ori = (fp*)malloc(sizeof(fp) * image_ori_elem);
	image = (fp*)malloc(sizeof(fp) * image_ori_elem);//Du
	
	int kk = 0;//Du
	for (kk=0; kk<image_ori_elem; kk++ )
	{
		image_ori[kk] = 2.9;
	}

	read_graphics(	"../../../data/srad/image.pgm",
								image_ori,
								Nr,
								Nc,
								1);
	


	time2 = omp_get_wtime();
	image = offload(niter, lambda, Nr, Nc, threads,image_ori_elem,image_ori,image);
	time3 = omp_get_wtime();



	write_graphics(	"image_out.pgm",
								image,
								Nr,
								Nc,
								1,
								255);

	time1 = omp_get_wtime();
	printf("Main Loop: %f s.\n", time3-time2);
	printf("Total time: %f s.\n", time1-time0);



}


