#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "ti_omp_device.h"
typedef struct float3 { float x, y, z; } float3;
extern void copy (float* dst, float* src, int N, int Threads);
extern void single_iteration(int, double *, double *, double *, int , int, double , double, double, double, double);
double stopwatch;
double duration;
#pragma omp end declare target


#define STR_SIZE	256
#define MAX_PD	3.0e6
#define PRECISION	0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
#define FACTOR_CHIP	0.5

#define t_chip 0.0005
#define chip_height 0.016
#define chip_width 0.016
#define amb_temp 80.0

void heap_init_msmc(char *p, size_t bytes) 
{ 
#pragma omp target map(to: bytes, p[0:bytes])
   {
      __heap_init_msmc(p,bytes); 
   }
}

void single_iteration(int num_th, double *result, double *temp, double *power, int row, int col,
					  double Cap, double Rx, double Ry, double Rz, 
					  double step)
{
	double delta;
	int r, c;
	stopwatch = omp_get_wtime();
	//printf("num_omp_threads: %d\n", num_omp_threads);
	#pragma omp parallel for shared(power, temp,result) private(r, c, delta) firstprivate(row, col) schedule(static) num_threads(num_th)
	for (r = 0; r < row; r++) {
		for (c = 0; c < col; c++) {
  			/*	Corner 1	*/
			if ( (r == 0) && (c == 0) ) {
				delta = (step / Cap) * (power[0] +
						(temp[1] - temp[0]) / Rx +
						(temp[col] - temp[0]) / Ry +
						(amb_temp - temp[0]) / Rz);
			}	/*	Corner 2	*/
			else if ((r == 0) && (c == col-1)) {
				delta = (step / Cap) * (power[c] +
						(temp[c-1] - temp[c]) / Rx +
						(temp[c+col] - temp[c]) / Ry +
						(amb_temp - temp[c]) / Rz);
			}	/*	Corner 3	*/
			else if ((r == row-1) && (c == col-1)) {
				delta = (step / Cap) * (power[r*col+c] + 
						(temp[r*col+c-1] - temp[r*col+c]) / Rx + 
						(temp[(r-1)*col+c] - temp[r*col+c]) / Ry + 
						(amb_temp - temp[r*col+c]) / Rz);					
			}	/*	Corner 4	*/
			else if ((r == row-1) && (c == 0)) {
				delta = (step / Cap) * (power[r*col] + 
						(temp[r*col+1] - temp[r*col]) / Rx + 
						(temp[(r-1)*col] - temp[r*col]) / Ry + 
						(amb_temp - temp[r*col]) / Rz);
			}	/*	Edge 1	*/
			else if (r == 0) {
				delta = (step / Cap) * (power[c] + 
						(temp[c+1] + temp[c-1] - 2.0*temp[c]) / Rx + 
						(temp[col+c] - temp[c]) / Ry + 
						(amb_temp - temp[c]) / Rz);
			}	/*	Edge 2	*/
			else if (c == col-1) {
				delta = (step / Cap) * (power[r*col+c] + 
						(temp[(r+1)*col+c] + temp[(r-1)*col+c] - 2.0*temp[r*col+c]) / Ry + 
						(temp[r*col+c-1] - temp[r*col+c]) / Rx + 
						(amb_temp - temp[r*col+c]) / Rz);
			}	/*	Edge 3	*/
			else if (r == row-1) {
				delta = (step / Cap) * (power[r*col+c] + 
						(temp[r*col+c+1] + temp[r*col+c-1] - 2.0*temp[r*col+c]) / Rx + 
						(temp[(r-1)*col+c] - temp[r*col+c]) / Ry + 
						(amb_temp - temp[r*col+c]) / Rz);
			}	/*	Edge 4	*/
			else if (c == 0) {
				delta = (step / Cap) * (power[r*col] + 
						(temp[(r+1)*col] + temp[(r-1)*col] - 2.0*temp[r*col]) / Ry + 
						(temp[r*col+1] - temp[r*col]) / Rx + 
						(amb_temp - temp[r*col]) / Rz);
			}	/*	Inside the chip	*/
			else {
				delta = (step / Cap) * (power[r*col+c] + 
						(temp[(r+1)*col+c] + temp[(r-1)*col+c] - 2.0*temp[r*col+c]) / Ry + 
						(temp[r*col+c+1] + temp[r*col+c-1] - 2.0*temp[r*col+c]) / Rx + 
						(amb_temp - temp[r*col+c]) / Rz);
			}
  			
			/*	Update Temperatures	*/
			result[r*col+c] =temp[r*col+c]+ delta;
		}
	}
	duration = duration + omp_get_wtime() - stopwatch;
	//omp_set_num_threads(num_omp_threads);
	stopwatch = omp_get_wtime();
	#pragma omp parallel for shared(result, temp) private(r, c) schedule(static) num_threads(num_th)
	for (r = 0; r < row; r++) {
		for (c = 0; c < col; c++) {
			temp[r*col+c]=result[r*col+c];
		}
	}
	duration = duration + omp_get_wtime() - stopwatch;

}


/* Transient solver driver routine: simply converts the heat 
 * transfer differential equations to difference equations 
 * and solves the difference equations by iterating
 */
double* compute_tran_temp(int num_th, double *result, int num_iterations, double *temp, double *power, int row, int col) 
{
	#pragma omp target map(to: num_th, result[0:row*col], num_iterations, power[0:row*col], row, col) map(tofrom:temp[0:row*col])
	{
		double time0, time1;
		time0 = omp_get_wtime();
		printf("hello from DSP\n");
		duration = 0;
		int i = 0;

		double *temp_msmc = (double*) __malloc_msmc(sizeof(double)*row*col);		
		double *result_msmc = (double*) __malloc_msmc(sizeof(double)*row*col);
		
		#pragma omp parallel for
		for (i=0; i<row*col; i++)
		{	
			temp_msmc[i] = temp[i];
			result_msmc[i] = result[i];
		}

		double grid_height = chip_height / row;
		double grid_width = chip_width / col;

		double Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
		double Rx = grid_width / (2.0 * K_SI * t_chip * grid_height);
		double Ry = grid_height / (2.0 * K_SI * t_chip * grid_width);
		double Rz = t_chip / (K_SI * grid_height * grid_width);

		double max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
		double step = PRECISION / max_slope;
		double t;


		for (i = 0; i < num_iterations ; i++)
		{
			single_iteration(num_th, result_msmc, temp_msmc, power, row, col, Cap, Rx, Ry, Rz, step);
		}

		#pragma omp parallel for
		for (i=0; i<row*col; i++)	
			temp[i] = temp_msmc[i];

		time1 = omp_get_wtime();
		printf("Duration in device: %f s.\n",time1-time0);
		printf("PR Time: %f s.\n",duration);


	} //end of offload
	return temp;
}


void LoopHere(int num_omp_threads )
{
	printf("Ready for offloading...\n");
	double time0 = omp_get_wtime();
	
	#pragma omp target map(to: num_omp_threads)
	{

	}//end of target

	double time1 = omp_get_wtime();
	printf("\nDuration in DSP: %f s.\n",time1-time0);
}

void runTest( int iterations)
{	
	#pragma omp target map(to: iterations)
	{	
		printf("hello from DSP\n");
	}
	//print out first few numbers to verify.
}
