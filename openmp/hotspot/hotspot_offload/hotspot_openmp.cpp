#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
using namespace std;
#define STR_SIZE	256

/* maximum power density possible (say 300W for a 10mm x 10mm chip)	*/
#define MAX_PD	(3.0e6)
/* required precision in degrees	*/
#define PRECISION	0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
/* capacitance fitting factor	*/
#define FACTOR_CHIP	0.5
#define OPEN
#define OUTPUT

/* chip parameters	*/
double t_chip = 0.0005;
double chip_height = 0.016;
double chip_width = 0.016;
/* ambient temperature, assuming no package at all	*/
double amb_temp = 80.0;

int num_omp_threads;

extern "C"{
	extern void runTest(int iterations);
	extern double* compute_tran_temp(int num_th, double *result, int num_iterations, double *temp, double *power, int row, int col);
}

/* Single iteration of the transient solver in the grid model.
 * advances the solution of the discretized difference equations 
 * by one time step
 */

void fatal(char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(1);
}

void read_input(double *vect, int grid_rows, int grid_cols, char *file)
{
  	int i, index;
	FILE *fp;
	char str[STR_SIZE];
	double val;

	fp = fopen (file, "r");
	if (!fp)
		fatal ("file could not be opened for reading");

	for (i=0; i < grid_rows * grid_cols; i++) {
		fgets(str, STR_SIZE, fp);
		if (feof(fp))
			fatal("not enough lines in file");
		if ((sscanf(str, "%lf", &val) != 1) )
			fatal("invalid file format");
		vect[i] = val;
	}

	fclose(fp);	
}

void usage(int argc, char **argv)
{
	fprintf(stderr, "Usage: %s <grid_rows> <grid_cols> <sim_time> <no. of threads><temp_file> <power_file>\n", argv[0]);
	fprintf(stderr, "\t<grid_rows>  - number of rows in the grid (positive integer)\n");
	fprintf(stderr, "\t<grid_cols>  - number of columns in the grid (positive integer)\n");
	fprintf(stderr, "\t<sim_time>   - number of iterations\n");
	fprintf(stderr, "\t<no. of threads>   - number of threads\n");
	fprintf(stderr, "\t<temp_file>  - name of the file containing the initial temperature values of each cell\n");
	fprintf(stderr, "\t<power_file> - name of the file containing the dissipated power values of each cell\n");
	exit(1);
}

int main(int argc, char **argv)
{
	int grid_rows, grid_cols, sim_time, i;
	double *temp, *power, *result;
	char *tfile, *pfile;
	double time0, time1, time2, time3, time4;
	time0 = omp_get_wtime();

	/* check validity of inputs	*/
	if (argc != 7)
		usage(argc, argv);
	if ((grid_rows = atoi(argv[1])) <= 0 ||
		(grid_cols = atoi(argv[2])) <= 0 ||
		(sim_time = atoi(argv[3])) <= 0 || 
		(num_omp_threads = atoi(argv[4])) <= 0
		)
		usage(argc, argv);

	printf("Launching...\niterations = %d, threads = %d\n",sim_time,num_omp_threads);


	/* allocate memory for the temperature and power arrays	*/
	temp = (double *) calloc (grid_rows * grid_cols, sizeof(double));
	power = (double *) calloc (grid_rows * grid_cols, sizeof(double));
	result = (double *) calloc (grid_rows * grid_cols, sizeof(double));
	if(!temp || !power)
		fatal("unable to allocate memory");

	/* read initial temperatures and input power*/
	tfile = argv[5];
	pfile = argv[6];

	time1 = omp_get_wtime();
	read_input(temp, grid_rows, grid_cols, tfile);
	read_input(power, grid_rows, grid_cols, pfile);
	
	time2 = omp_get_wtime();
	////
	printf("Start computing the transient temperature...\n");
	temp = compute_tran_temp(num_omp_threads, result, sim_time, temp, power, grid_rows, grid_cols);

	////
	printf("Ending simulation...\n");
	time3 = omp_get_wtime();

	/* output results	*/
#ifdef VERBOSE
	fprintf(stdout, "Final Temperatures:\n");
#endif

#ifdef OUTPUT
	FILE *fp = fopen("result.txt","w");
	for(i=0; i<grid_rows * grid_cols; i++)
		fprintf(fp,"%d\t%g\n",i,temp[i]);
	fclose(fp);
#endif
	/* cleanup	*/
	free(temp);
	free(power);

	time4 = omp_get_wtime();

	printf("Primary Loop: %f s.\n",time3-time2);
	printf("Total Time: %f s.\n",time4-time0);

	return 0;
}

