/*

offload target file

*/

#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
extern long long get_time(void);
#pragma omp end declare target

long long get_time(void){
	long long ts;
	ts = (long long) (1000000*omp_get_wtime());
	return ts;
}


int transpose(float ** A, float ** B, int N, int nthreads, int niter)

{
	float * A_arr, * B_arr;
	A_arr = (float*) malloc(N*N*sizeof(float));
	B_arr = (float*) malloc(N*N*sizeof(float));
	int i,j;
	
	for (i=0;i<N;i++)
	for (j=0;j<N;j++)
	{
		A_arr[i*N+j] = A[i][j];
	}

	#pragma omp target map(to:N, nthreads, niter) map(tofrom: A_arr[0:N*N], B_arr[0:N*N])
	{
		long long time0;
		long long time1;
		time0 = get_time();
		double stopwatch;
		double duration = 0;

		int i,j;
		printf("num of threads = %d\n", nthreads);
		
		float **a, **b;
		a = (float**) malloc(N*sizeof(float*));
		b = (float**) malloc(N*sizeof(float*));
		for (i=0; i<N; i++)
		{
			a[i] = (float*) malloc(N*sizeof(float));
			b[i] = (float*) malloc(N*sizeof(float));
		}
			
		for (i=0; i<N; i++)
		for (j=0; j<N; j++)
			a[i][j] = A_arr[i*N+j];
		

		stopwatch = omp_get_wtime();

		int count = niter;
		while (count>0)
	{		
		#pragma omp parallel for \
				private(j) shared(a,b) num_threads(nthreads)
		for (i=0; i<N; i++)
			for (j=0; j<N; j++)
				b[j][i] = a[i][j];
		count--;
	}	
		duration = omp_get_wtime() - stopwatch;//Du
	
		for (i=0; i<N; i++)
                for (j=0; j<N; j++)
                        B_arr[i*N+j] = b[i][j];

		
		time1 = get_time();
		printf("\nTime consumed offload: %lf ms \n", (float) (time1-time0) / 1000);
		printf("PR Time: %f s.\n",duration);//Du

	}	// end of offload


	for (i=0; i<N; i++)
              	for (j=0; j<N; j++)
		{
		B[i][j] = B_arr[i*N+j];
	//	printf("%f\t",B[i][j]);
		}

	return 0;
}
