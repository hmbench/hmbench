#define LIMIT -999
//#define TRACE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
//#define OPENMP
//#define NUM_THREAD 4
//#define TRACKBACK

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
extern "C"{
	extern void runTest( int iterations, int max_rows, int max_cols, int penalty, int omp_num_threads);//Du
}

double gettime() {
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec+t.tv_usec*1e-6;
}


void usage(int argc, char **argv)
{
	fprintf(stderr, "Usage: %s <max_rows/max_cols> <penalty> <num_threads>\n", argv[0]);
	fprintf(stderr, "\t<dimension>      - x and y dimensions\n");
	fprintf(stderr, "\t<penalty>        - penalty(positive integer)\n");
	fprintf(stderr, "\t<num_threads>    - no. of threads\n");
	fprintf(stderr, "\t<iterations>    - no. of iters\n");
	exit(1);

}


////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv) 
{
	double time0 = omp_get_wtime();

	int max_rows, max_cols, penalty, omp_num_threads,iterations;
	// the lengths of the two sequences should be able to divided by 16.
	// And at current stage  max_rows needs to equal max_cols
	if (argc == 5)
	{
		max_rows = atoi(argv[1]);
		max_cols = atoi(argv[1]);
		if (max_cols>1280)
		{
			printf("We don't recommend using dimension greater than 1280.\n Reset to 1280.");
			max_rows = 1280;
			max_cols = 1280;
		}
		penalty = atoi(argv[2]);
		omp_num_threads = atoi(argv[3]);
		iterations = atoi(argv[4]);
	}
    else{
		usage(argc, argv);
    }
	
	printf("\nReady for offloading...\niterations = %d\nthreads = %d\n", iterations, omp_num_threads);

	double time1 = omp_get_wtime();
    runTest(iterations, max_rows, max_cols, penalty, omp_num_threads);
	double time2 = omp_get_wtime();
	double time3 = omp_get_wtime();

	printf("Main Loop: %f s.\n",time2-time1);
	printf("Total Time: %f s.\n",time3-time0);


    return EXIT_SUCCESS;
}



