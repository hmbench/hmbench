#define LIMIT -999
//#define TRACE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
//#include <omp.h>
//#define OPENMP
//#define NUM_THREAD 4
//#define TRACKBACK

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
extern "C"{
	extern void runTest( int max_rows, int max_cols, int penalty, int omp_num_threads);//Du
}//be aware that the host file is in .cpp

double gettime() {
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec+t.tv_usec*1e-6;
}


void usage(int argc, char **argv)
{
	fprintf(stderr, "Usage: %s <max_rows/max_cols> <penalty> <num_threads>\n", argv[0]);
	fprintf(stderr, "\t<dimension>      - x and y dimensions\n");
	fprintf(stderr, "\t<penalty>        - penalty(positive integer)\n");
	fprintf(stderr, "\t<num_threads>    - no. of threads\n");
	exit(1);
}


////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv) 
{
	int max_rows, max_cols, penalty, omp_num_threads;
	// the lengths of the two sequences should be able to divided by 16.
	// And at current stage  max_rows needs to equal max_cols
	if (argc == 4)
	{
		max_rows = atoi(argv[1]);
		max_cols = atoi(argv[1]);
		if (max_cols>1280)
		{
			printf("We don't recommend using dimension greater than 1280.\n Reset to 1280.");
			max_rows = 1280;
			max_cols = 1280;
		}
		penalty = atoi(argv[2]);
		omp_num_threads = atoi(argv[3]);
	}
    else{
		usage(argc, argv);
    }
	
	printf("\nReady for offloading...\n");

	double time0 = gettime();
    runTest(max_rows, max_cols, penalty, omp_num_threads);
	double time1 = gettime();

	printf("Duration: %f s.\n",time1-time0);

//
////#define TRACEBACK
//#ifdef TRACEBACK
//	
//	FILE *fpo = fopen("result.txt","w");
//	fprintf(fpo, "print traceback value GPU:\n");
//    
//	for (int i = max_rows - 2,  j = max_rows - 2; i>=0, j>=0;){
//		int nw, n, w, traceback;
//		if ( i == max_rows - 2 && j == max_rows - 2 )
//			fprintf(fpo, "%d ", input_itemsets[ i * max_cols + j]); //print the first element
//		if ( i == 0 && j == 0 )
//           break;
//		if ( i > 0 && j > 0 ){
//			nw = input_itemsets[(i - 1) * max_cols + j - 1];
//		    w  = input_itemsets[ i * max_cols + j - 1 ];
//            n  = input_itemsets[(i - 1) * max_cols + j];
//		}
//		else if ( i == 0 ){
//		    nw = n = LIMIT;
//		    w  = input_itemsets[ i * max_cols + j - 1 ];
//		}
//		else if ( j == 0 ){
//		    nw = w = LIMIT;
//            n  = input_itemsets[(i - 1) * max_cols + j];
//		}
//		else{
//		}
//
//		//traceback = maximum(nw, w, n);
//		int new_nw, new_w, new_n;
//		new_nw = nw + referrence[i * max_cols + j];
//		new_w = w - penalty;
//		new_n = n - penalty;
//		
//		traceback = maximum(new_nw, new_w, new_n);
//		if(traceback == new_nw)
//			traceback = nw;
//		if(traceback == new_w)
//			traceback = w;
//		if(traceback == new_n)
//            traceback = n;
//			
//		fprintf(fpo, "%d ", traceback);
//
//		if(traceback == nw )
//		{i--; j--; continue;}
//
//        else if(traceback == w )
//		{j--; continue;}
//
//        else if(traceback == n )
//		{i--; continue;}
//
//		else
//		;
//	}
//	
//	fclose(fpo);
//#endif

    return EXIT_SUCCESS;
}



