
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <assert.h>
#include <omp.h>
#include <time.h>


extern int* 
transpose(float** A, float ** B, int N, int nthreads, int niter);


int main ( int argc, char *argv[] )
{
  	int N = 128; /* default size */
  	int nthreads = 2; // by default
	int niter = 1000;

	if(argc != 4){
                printf("ERROR: wrong number of arguments\n");
                printf("[dimension] [threads] [niter]\n");
                return 0;
        }
        else{
                N = atoi(argv[1]);
                nthreads = atoi(argv[2]);
                niter = atoi(argv[3]);
                printf("dimension = %d, threads = %d, niter = %d\n",N,nthreads,niter);
        }


	int i,j;  

	double time0 = omp_get_wtime();

	float ** A, **B;
	A = (float**) malloc(N*sizeof(float*));
	B = (float**) malloc(N*sizeof(float*));
	for (i=0;i<N;i++)
	{
		A[i] = (float*) malloc(N*sizeof(float));
		B[i] = (float*) malloc(N*sizeof(float));
	}	
	srand(9);
	for (i=0;i<N;i++)
		for (j=0;j<N;j++)
	{		
			A[i][j] = rand()*1.0;
	//		printf("%f\t",A[i][j]);
	}


	double time1 = omp_get_wtime();
	int m = transpose(A, B, N, nthreads, niter);
	double time2 = omp_get_wtime();

	
	//printf("debug 1\n");
	//printf("debug 2\n");

	double time3 = omp_get_wtime();

	printf("Main Loop: %f s.\n",time2-time1);
	printf("Total Time: %f s.\n",time3-time0);

}				/* ----------  end of function main  ---------- */
