#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>
//#define NUM_THREAD 4
#define OPEN

int no_of_nodes;
int edge_list_size;
FILE *fp;

//Structure to hold a node information
struct Node
{
	int starting;
	int no_of_edges;
};


extern "C"{
	extern void runTest(int iterations);
	extern int* LoopHere(int iterations, int num_omp_threads, int no_of_nodes, int edge_list_size, Node* h_graph_nodes, int* h_cost, int* h_graph_edges);
}


void BFSGraph(int argc, char** argv);

void Usage(int argc, char**argv){
	fprintf(stderr,"Usage: %s <num_threads> <input_file>\n", argv[0]);
}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv) 
{
	no_of_nodes=0;
	edge_list_size=0;
	BFSGraph( argc, argv);
}


////////////////////////////////////////////////////////////////////////////////
//Apply BFS on a Graph using CUDA
////////////////////////////////////////////////////////////////////////////////
void BFSGraph( int argc, char** argv) 
{
    char *input_f;
	int	 num_omp_threads;
	int iterations;
	double time0, time1, time2, time3, time4, time5;
	time0 = omp_get_wtime();

	if(argc!=4){
	Usage(argc, argv);
	exit(0);
	}
    
	num_omp_threads = atoi(argv[1]);
	input_f = argv[2];
	iterations = atoi(argv[3]);
	
	printf("\nReading File...\n");
	//Read in Graph from a file
	fp = fopen(input_f,"r");
	if(!fp)
	{
		printf("Error Reading graph file\n");
		return;
	}

	int source = 0;

	fscanf(fp,"%d",&no_of_nodes);
	time1 = omp_get_wtime();

   
	// allocate host memory
	Node* h_graph_nodes = (Node*) malloc(sizeof(Node)*no_of_nodes);

	int start, edgeno;   
	// initalize the memory
	for( unsigned int i = 0; i < no_of_nodes; i++) 
	{
		fscanf(fp,"%d %d",&start,&edgeno);
		h_graph_nodes[i].starting = start;
		h_graph_nodes[i].no_of_edges = edgeno;
	}

	//read the source node from the file
	fscanf(fp,"%d",&source);
	fscanf(fp,"%d",&edge_list_size);

	source=0;
	
	time2 = omp_get_wtime();

	//set the source node as true in the mask
	
	int id,cost;
	int* h_graph_edges = (int*) malloc(sizeof(int)*edge_list_size);
	for(int i=0; i < edge_list_size ; i++)
	{
		fscanf(fp,"%d",&id);
		fscanf(fp,"%d",&cost);
		h_graph_edges[i] = id;
	}

	if(fp)
		fclose(fp);


	// allocate mem for the result on host side
	int* h_cost = (int*) malloc( sizeof(int)*no_of_nodes);


	time3 = omp_get_wtime();
	
	h_cost = LoopHere(iterations, num_omp_threads, no_of_nodes, edge_list_size, h_graph_nodes, h_cost, h_graph_edges);
	

	
	time4 = omp_get_wtime();

	//Store the result into a file
	FILE *fpo = fopen("result.txt","w");
	for(int i=0;i<no_of_nodes;i++)
		fprintf(fpo,"%d) %d\n",i,h_cost[i]);

	fclose(fpo);
	printf("\n");

	// cleanup memory
	free( h_graph_nodes);
	free( h_graph_edges);
	free( h_cost);

	time5 = omp_get_wtime();
	
	printf("file read: %f\n",time3-time2);
	printf("primary loop: %f\n",time4-time3);
	printf("Total Time: %f\n",time5-time0);

}

