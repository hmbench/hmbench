#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
typedef struct float3 { float x, y, z; } float3;
extern void copy (float* dst, float* src, int N, int Threads);
extern void compute_step_factor (int nelr, float* variables, float* areas, float* step_factors, int num_th);
extern void compute_flux (int nelr, int* elements_surrounding_elements, float* normals, float* variables, float* fluxes, int num_th, float* ff_variable, float3 ff_flux_contribution_momentum_x, float3 ff_flux_contribution_momentum_y, float3 ff_flux_contribution_momentum_z, float3 ff_flux_contribution_density_energy);
extern void time_step(int j, int nelr, float* old_variables, float* variables, float* step_factors, float* fluxes, int num_th);
extern inline void compute_velocity(float, float3, float3);
extern inline float compute_speed_sqd(float3);
extern inline float compute_pressure(float, float, float);
extern inline float compute_speed_of_sound(float, float);
extern inline void compute_flux_contribution(float, float3, float, float, float3, float3, float3, float3, float3);
double stopwatch;
double duration;
#pragma omp end declare target

#define GAMMA 1.4
#define NDIM 3
#define NNB 4
#define RK 3	// 3rd order RK
#define ff_mach 1.2
#define deg_angle_of_attack 0.0f
#define VAR_DENSITY 0
#define VAR_MOMENTUM  1
#define VAR_DENSITY_ENERGY (VAR_MOMENTUM+NDIM)
#define NVAR (VAR_DENSITY_ENERGY+1)

//typedef struct float3 { float x, y, z; } float3;

void copy(float* dst, float* src, int N, int num_th)
{
	//printf("Here1: \n");
	
	int i;
	stopwatch = omp_get_wtime();
	#pragma omp parallel for default(shared) schedule(static) num_threads(num_th)
	for( i = 0; i < N; i++)
	{
		dst[i] = src[i];
	}
	duration = duration + omp_get_wtime() - stopwatch;
}

inline void compute_flux_contribution(float density, float3 momentum, float density_energy, float pressure, float3 velocity, float3 fc_momentum_x, float3 fc_momentum_y, float3 fc_momentum_z, float3 fc_density_energy)
{
	fc_momentum_x.x = velocity.x*momentum.x + pressure;
	fc_momentum_x.y = velocity.x*momentum.y;
	fc_momentum_x.z = velocity.x*momentum.z;

	fc_momentum_y.x = fc_momentum_x.y;
	fc_momentum_y.y = velocity.y*momentum.y + pressure;
	fc_momentum_y.z = velocity.y*momentum.z;

	fc_momentum_z.x = fc_momentum_x.z;
	fc_momentum_z.y = fc_momentum_y.z;
	fc_momentum_z.z = velocity.z*momentum.z + pressure;

	float de_p = density_energy + pressure;
	fc_density_energy.x = velocity.x*de_p;
	fc_density_energy.y = velocity.y*de_p;
	fc_density_energy.z = velocity.z*de_p;
}

inline void compute_velocity(float density, float3 momentum, float3 velocity)
{
	velocity.x = momentum.x / density;
	velocity.y = momentum.y / density;
	velocity.z = momentum.z / density;
}

inline float compute_speed_sqd(float3 velocity)
{
	return velocity.x*velocity.x + velocity.y*velocity.y + velocity.z*velocity.z;
}

inline float compute_pressure(float density, float density_energy, float speed_sqd)
{
	return ((float)(GAMMA)-(float)(1.0f))*(density_energy - (float)(0.5f)*density*speed_sqd);
}

inline float compute_speed_of_sound(float density, float pressure)
{
	return sqrt((float)(GAMMA)*pressure/density);
}	

void compute_step_factor(int nelr, float* variables, float* areas, float* step_factors, int num_th)
{
	int i;
	stopwatch = omp_get_wtime();

	#pragma omp parallel for default(shared) schedule(static) num_threads(num_th)
	for(i = 0; i < nelr; i++)
	{
		float density = variables[NVAR*i + VAR_DENSITY];

		float3 momentum;
		momentum.x = variables[NVAR*i + (VAR_MOMENTUM+0)];
		momentum.y = variables[NVAR*i + (VAR_MOMENTUM+1)];
		momentum.z = variables[NVAR*i + (VAR_MOMENTUM+2)];

		float density_energy = variables[NVAR*i + VAR_DENSITY_ENERGY];
		float3 velocity;	   
		compute_velocity(density, momentum, velocity);
		float speed_sqd      = compute_speed_sqd(velocity);
		float pressure       = compute_pressure(density, density_energy, speed_sqd);
		float speed_of_sound = compute_speed_of_sound(density, pressure);

		// dt = float(0.5f) * std::sqrt(areas[i]) /  (||v|| + c).... but when we do time stepping, this later would need to be divided by the area, so we just do it all at once
		step_factors[i] = (float)(0.5f) / (sqrt(areas[i]) * (sqrt(speed_sqd) + speed_of_sound));
	}

	duration = duration + omp_get_wtime() - stopwatch;
}


void time_step(int j, int nelr, float* old_variables, float* variables, float* step_factors, float* fluxes, int num_th)
{
	int i;
	stopwatch = omp_get_wtime();

	#pragma omp parallel for default(shared) schedule(static) num_threads(num_th)
	for(i = 0; i < nelr; i++)
	{
		float factor = step_factors[i]/(float)(RK+1-j);

		variables[NVAR*i + VAR_DENSITY] = old_variables[NVAR*i + VAR_DENSITY] + factor*fluxes[NVAR*i + VAR_DENSITY];
		variables[NVAR*i + VAR_DENSITY_ENERGY] = old_variables[NVAR*i + VAR_DENSITY_ENERGY] + factor*fluxes[NVAR*i + VAR_DENSITY_ENERGY];
		variables[NVAR*i + (VAR_MOMENTUM+0)] = old_variables[NVAR*i + (VAR_MOMENTUM+0)] + factor*fluxes[NVAR*i + (VAR_MOMENTUM+0)];
		variables[NVAR*i + (VAR_MOMENTUM+1)] = old_variables[NVAR*i + (VAR_MOMENTUM+1)] + factor*fluxes[NVAR*i + (VAR_MOMENTUM+1)];
		variables[NVAR*i + (VAR_MOMENTUM+2)] = old_variables[NVAR*i + (VAR_MOMENTUM+2)] + factor*fluxes[NVAR*i + (VAR_MOMENTUM+2)];
	}
	duration = duration + omp_get_wtime() - stopwatch;
}


void compute_flux(int nelr, int* elements_surrounding_elements, float* normals, float* variables, float* fluxes, int num_th, float* ff_variable, float3 ff_flux_contribution_momentum_x, float3 ff_flux_contribution_momentum_y, float3 ff_flux_contribution_momentum_z, float3 ff_flux_contribution_density_energy)
{
	const float smoothing_coefficient = (float)(0.2f);
	int i;
	stopwatch = omp_get_wtime();
	#pragma omp parallel for default(shared) schedule(static) num_threads(num_th)
	for(i = 0; i < nelr; i++)
	{
		int j, nb;
		float3 normal; float normal_len;
		float factor;

		float density_i = variables[NVAR*i + VAR_DENSITY];
		float3 momentum_i;
		momentum_i.x = variables[NVAR*i + (VAR_MOMENTUM+0)];
		momentum_i.y = variables[NVAR*i + (VAR_MOMENTUM+1)];
		momentum_i.z = variables[NVAR*i + (VAR_MOMENTUM+2)];

		float density_energy_i = variables[NVAR*i + VAR_DENSITY_ENERGY];

		float3 velocity_i;             				 
		compute_velocity(density_i, momentum_i, velocity_i);
		float speed_sqd_i                          = compute_speed_sqd(velocity_i);
		float speed_i                              = sqrt(speed_sqd_i);
		float pressure_i                           = compute_pressure(density_i, density_energy_i, speed_sqd_i);
		float speed_of_sound_i                     = compute_speed_of_sound(density_i, pressure_i);
		float3 flux_contribution_i_momentum_x, flux_contribution_i_momentum_y, flux_contribution_i_momentum_z;
		float3 flux_contribution_i_density_energy;
		compute_flux_contribution(density_i, momentum_i, density_energy_i, pressure_i, velocity_i, flux_contribution_i_momentum_x, flux_contribution_i_momentum_y, flux_contribution_i_momentum_z, flux_contribution_i_density_energy);

		float flux_i_density = (float)(0.0f);
		float3 flux_i_momentum;
		flux_i_momentum.x = (float)(0.0f);
		flux_i_momentum.y = (float)(0.0f);
		flux_i_momentum.z = (float)(0.0f);
		float flux_i_density_energy = (float)(0.0f);

		float3 velocity_nb;
		float density_nb, density_energy_nb;
		float3 momentum_nb;
		float3 flux_contribution_nb_momentum_x, flux_contribution_nb_momentum_y, flux_contribution_nb_momentum_z;
		float3 flux_contribution_nb_density_energy;
		float speed_sqd_nb, speed_of_sound_nb, pressure_nb;

		for(j = 0; j < NNB; j++)
		{
			nb = elements_surrounding_elements[i*NNB + j];
			normal.x = normals[(i*NNB + j)*NDIM + 0];
			normal.y = normals[(i*NNB + j)*NDIM + 1];
			normal.z = normals[(i*NNB + j)*NDIM + 2];
			normal_len = sqrt(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);

			if(nb >= 0) 	// a legitimate neighbor
			{
				density_nb =        variables[nb*NVAR + VAR_DENSITY];
				momentum_nb.x =     variables[nb*NVAR + (VAR_MOMENTUM+0)];
				momentum_nb.y =     variables[nb*NVAR + (VAR_MOMENTUM+1)];
				momentum_nb.z =     variables[nb*NVAR + (VAR_MOMENTUM+2)];
				density_energy_nb = variables[nb*NVAR + VAR_DENSITY_ENERGY];
													compute_velocity(density_nb, momentum_nb, velocity_nb);
				speed_sqd_nb                      = compute_speed_sqd(velocity_nb);
				pressure_nb                       = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
				speed_of_sound_nb                 = compute_speed_of_sound(density_nb, pressure_nb);
				compute_flux_contribution(density_nb, momentum_nb, density_energy_nb, pressure_nb, velocity_nb, flux_contribution_nb_momentum_x, flux_contribution_nb_momentum_y, flux_contribution_nb_momentum_z, flux_contribution_nb_density_energy);

				// artificial viscosity
				factor = -normal_len*smoothing_coefficient*(float)(0.5f)*(speed_i + sqrt(speed_sqd_nb) + speed_of_sound_i + speed_of_sound_nb);
				flux_i_density += factor*(density_i-density_nb);
				flux_i_density_energy += factor*(density_energy_i-density_energy_nb);
				flux_i_momentum.x += factor*(momentum_i.x-momentum_nb.x);
				flux_i_momentum.y += factor*(momentum_i.y-momentum_nb.y);
				flux_i_momentum.z += factor*(momentum_i.z-momentum_nb.z);

				// accumulate cell-centered fluxes
				factor = (float)(0.5f)*normal.x;
				flux_i_density += factor*(momentum_nb.x+momentum_i.x);
				flux_i_density_energy += factor*(flux_contribution_nb_density_energy.x+flux_contribution_i_density_energy.x);
				flux_i_momentum.x += factor*(flux_contribution_nb_momentum_x.x+flux_contribution_i_momentum_x.x);
				flux_i_momentum.y += factor*(flux_contribution_nb_momentum_y.x+flux_contribution_i_momentum_y.x);
				flux_i_momentum.z += factor*(flux_contribution_nb_momentum_z.x+flux_contribution_i_momentum_z.x);

				factor = (float)(0.5f)*normal.y;
				flux_i_density += factor*(momentum_nb.y+momentum_i.y);
				flux_i_density_energy += factor*(flux_contribution_nb_density_energy.y+flux_contribution_i_density_energy.y);
				flux_i_momentum.x += factor*(flux_contribution_nb_momentum_x.y+flux_contribution_i_momentum_x.y);
				flux_i_momentum.y += factor*(flux_contribution_nb_momentum_y.y+flux_contribution_i_momentum_y.y);
				flux_i_momentum.z += factor*(flux_contribution_nb_momentum_z.y+flux_contribution_i_momentum_z.y);

				factor = (float)(0.5f)*normal.z;
				flux_i_density += factor*(momentum_nb.z+momentum_i.z);
				flux_i_density_energy += factor*(flux_contribution_nb_density_energy.z+flux_contribution_i_density_energy.z);
				flux_i_momentum.x += factor*(flux_contribution_nb_momentum_x.z+flux_contribution_i_momentum_x.z);
				flux_i_momentum.y += factor*(flux_contribution_nb_momentum_y.z+flux_contribution_i_momentum_y.z);
				flux_i_momentum.z += factor*(flux_contribution_nb_momentum_z.z+flux_contribution_i_momentum_z.z);
			}
			else if(nb == -1)	// a wing boundary
			{
				flux_i_momentum.x += normal.x*pressure_i;
				flux_i_momentum.y += normal.y*pressure_i;
				flux_i_momentum.z += normal.z*pressure_i;
			}
			else if(nb == -2) // a far field boundary
			{
				factor = (float)(0.5f)*normal.x;
				flux_i_density += factor*(ff_variable[VAR_MOMENTUM+0]+momentum_i.x);
				flux_i_density_energy += factor*(ff_flux_contribution_density_energy.x+flux_contribution_i_density_energy.x);
				flux_i_momentum.x += factor*(ff_flux_contribution_momentum_x.x + flux_contribution_i_momentum_x.x);
				flux_i_momentum.y += factor*(ff_flux_contribution_momentum_y.x + flux_contribution_i_momentum_y.x);
				flux_i_momentum.z += factor*(ff_flux_contribution_momentum_z.x + flux_contribution_i_momentum_z.x);

				factor = (float)(0.5f)*normal.y;
				flux_i_density += factor*(ff_variable[VAR_MOMENTUM+1]+momentum_i.y);
				flux_i_density_energy += factor*(ff_flux_contribution_density_energy.y+flux_contribution_i_density_energy.y);
				flux_i_momentum.x += factor*(ff_flux_contribution_momentum_x.y + flux_contribution_i_momentum_x.y);
				flux_i_momentum.y += factor*(ff_flux_contribution_momentum_y.y + flux_contribution_i_momentum_y.y);
				flux_i_momentum.z += factor*(ff_flux_contribution_momentum_z.y + flux_contribution_i_momentum_z.y);

				factor = (float)(0.5f)*normal.z;
				flux_i_density += factor*(ff_variable[VAR_MOMENTUM+2]+momentum_i.z);
				flux_i_density_energy += factor*(ff_flux_contribution_density_energy.z+flux_contribution_i_density_energy.z);
				flux_i_momentum.x += factor*(ff_flux_contribution_momentum_x.z + flux_contribution_i_momentum_x.z);
				flux_i_momentum.y += factor*(ff_flux_contribution_momentum_y.z + flux_contribution_i_momentum_y.z);
				flux_i_momentum.z += factor*(ff_flux_contribution_momentum_z.z + flux_contribution_i_momentum_z.z);

			}
		}

		fluxes[i*NVAR + VAR_DENSITY] = flux_i_density;
		fluxes[i*NVAR + (VAR_MOMENTUM+0)] = flux_i_momentum.x;
		fluxes[i*NVAR + (VAR_MOMENTUM+1)] = flux_i_momentum.y;
		fluxes[i*NVAR + (VAR_MOMENTUM+2)] = flux_i_momentum.z;
		fluxes[i*NVAR + VAR_DENSITY_ENERGY] = flux_i_density_energy;
	}
	duration = duration + omp_get_wtime() - stopwatch;
}


float* runTest( int iterations, int num_th, int nelr, float* normals, float* old_variables, float* variables, float* areas, float* step_factors, int* elements_surrounding_elements, float* fluxes, float* ff_variable, float3 ff_flux_contribution_momentum_x, float3 ff_flux_contribution_momentum_y, float3 ff_flux_contribution_momentum_z, float3 ff_flux_contribution_density_energy)
{	
	#pragma omp target map(to: iterations, num_th, nelr, normals[0:NDIM*NNB*nelr], old_variables[0:nelr*NVAR], areas[0:nelr], step_factors[0:nelr], elements_surrounding_elements[0:nelr*NNB], fluxes[0:nelr*NVAR], ff_variable[0:NVAR], ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z, ff_flux_contribution_density_energy) map(tofrom:variables[0:nelr*NVAR])
	{	
		int i,j;
		double time0, time1;
		time0 = omp_get_wtime();
		duration = 0;

		for(i = 0; i < iterations; i++)
		{
			copy(old_variables, variables, nelr*NVAR, num_th);

			// for the first iteration we compute the time step
			compute_step_factor(nelr, variables, areas, step_factors, /*Du*/num_th);
			
			for(j = 0; j < RK; j++)
			{
				compute_flux ( nelr, elements_surrounding_elements, normals, variables, fluxes, num_th, ff_variable, ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z, ff_flux_contribution_density_energy);
				time_step(j, nelr, old_variables, variables, step_factors, fluxes, num_th);
			}
		}

		time1 = omp_get_wtime();
		printf("Duration in DSP: %f s.\n",time1-time0);
		printf("PR Time: %f s.\n",duration);
		//print out first few numbers to verify.
	}
	//print out first few numbers to verify.
	return variables;
}
