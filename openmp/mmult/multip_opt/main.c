
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <assert.h>
#include <omp.h>
#include <time.h>
#include "ti_omp_device.h"

extern int 
	multiply(float** A, float ** B, float ** C, int N1, int N2, int N3, int nthreads, int niter);

extern void heap_init_msmc(char *, size_t);


int main (int argc, char *argv[] )
{
  	int N1 = 128; /* default size */
  	int N2 = 12; /* default size */
  	int N3 = 18; /* default size */
  	int nthreads = 2; // by default
	int niter = 1000;

	if(argc != 6){
                printf("ERROR: wrong number of arguments\n");
                printf("[dimension1] [dimension2] [dimension3] [threads] [niter]\n");
                return 0;
        }
        else{
                N1 = atoi(argv[1]);
                N2 = atoi(argv[2]);
                N3 = atoi(argv[3]);
                nthreads = atoi(argv[4]);
                niter = atoi(argv[5]);
                printf("dimension = %d, %d, %d, threads = %d, niter = %d \n", N1, N2, N3, nthreads, niter);
        }

	int i,j;  
	
	int HeapSize = 3 * (1<<20);//Du
        char* HeapMSMC = (char*) __malloc_msmc(HeapSize);//Du
        heap_init_msmc(HeapMSMC, HeapSize);//Du

	double time0 = omp_get_wtime();


	float ** A, **B, **C;
	A = (float**) malloc(N1*sizeof(float*));
	B = (float**) malloc(N2*sizeof(float*));
	C = (float**) malloc(N1*sizeof(float*));
	for (i=0;i<N1;i++)
	{
		A[i] = (float*) malloc(N2*sizeof(float));
		C[i] = (float*) malloc(N3*sizeof(float));
	}
	for (i=0;i<N2;i++)
		B[i] = (float*) malloc(N3*sizeof(float));
		

	srand(9);
	for (i=0;i<N1;i++)
		for (j=0;j<N2;j++)
			A[i][j] = rand()*1.0;
	for (i=0; i<N2; i++)
		for (j=0;j<N3;j++)
			B[i][j] = rand()*1.1;

	double time1 = omp_get_wtime();
	int m = multiply(A, B, C, N1, N2, N3, nthreads, niter);
	double time2 = omp_get_wtime();


	double time3 = omp_get_wtime();

	printf("Main Loop: %f s.\n",time2-time1);
	printf("Total Time: %f s.\n",time3-time0);

}				/* ----------  end of function main  ---------- */
