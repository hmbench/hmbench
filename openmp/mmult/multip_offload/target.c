/*

offload target file

*/

#pragma omp declare target
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
extern long long get_time(void);
#pragma omp end declare target

long long get_time(void){
	long long ts;
	ts = (long long) (1000000*omp_get_wtime());
	return ts;
}


int multiply(float ** A, float ** B, float **C, int N1, int N2, int N3, int nthreads, int niter)

{
	float * A_arr, * B_arr, *C_arr;
	A_arr = (float*) malloc(N1*N2*sizeof(float));
	B_arr = (float*) malloc(N2*N3*sizeof(float));
	C_arr = (float*) malloc(N1*N3*sizeof(float));
	int i,j,k;
	
	for (i=0;i<N1;i++)
	for (j=0;j<N2;j++)
		A_arr[i*N2+j] = A[i][j];

	for (i=0;i<N2;i++)
	for (j=0;j<N3;j++)
		B_arr[i*N3+j] = B[i][j];

	#pragma omp target map(to:N1, N2, N3, nthreads, niter, A_arr[0:N1*N2], B_arr[0:N2*N3])  map(tofrom: C_arr[0:N1*N3])
	{
		long long time0;
		long long time1;
		time0 = get_time();
		double stopwatch;
		double duration = 0;

		int i,j;
		printf("num of threads = %d\n", nthreads);
	
		float ** a, **b, **c;
	        a = (float**) malloc(N1*sizeof(float*));
        	b = (float**) malloc(N2*sizeof(float*));
        	c = (float**) malloc(N1*sizeof(float*));
        	for (i=0;i<N1;i++)
        	{
                	a[i] = (float*) malloc(N2*sizeof(float));
                	c[i] = (float*) malloc(N3*sizeof(float));
        	}
        	for (i=0;i<N2;i++)
                	b[i] = (float*) malloc(N3*sizeof(float));

		for (i=0; i<N1; i++)
		for (j=0; j<N2; j++)
			a[i][j] = A_arr[i*N2+j];
	
		for (i=0; i<N2; i++)
                for (j=0; j<N3; j++)
                        b[i][j] = B_arr[i*N3+j];
	
		stopwatch = omp_get_wtime();

		int count = niter;
		while (count>0)
	{		
		#pragma omp parallel for \
				private(j,k) shared(a,b,c) num_threads(nthreads)
		for (i=0; i<N1; i++)
		for (j=0; j<N3; j++)
		for (k=0; k<N2; k++)
			c[i][j] += a[i][k] * b[k][j];
		count--;
	}	
		duration = omp_get_wtime() - stopwatch;//Du
	
		for (i=0; i<N1; i++)
                for (j=0; j<N3; j++)
                        C_arr[i*N3+j] = c[i][j];

		
		time1 = get_time();
		printf("\nTime consumed offload: %lf ms \n", (float) (time1-time0) / 1000);
		printf("PR Time: %f s.\n",duration);	

	}	// end of offload


	for (i=0; i<N1; i++)
              	for (j=0; j<N3; j++)
		{
		C[i][j] = C_arr[i*N3+j];
		}

	return 0;
}
