#include <stdio.h>					// (in path known to compiler)			needed by printf
#include <stdlib.h>					// (in path known to compiler)			needed by malloc
#include <stdbool.h>				// (in path known to compiler)			needed by true/false
#include <omp.h>
#include "ti_omp_device.h"
#include "./util/timer/timer.h"			// (in path specified here)
#include "./util/num/num.h"				// (in path specified here)
#include "./main.h"						// (in the current directory)

//========================================================================================================================================================================================================200
//	MAIN FUNCTION
//========================================================================================================================================================================================================200


extern void OffLoad(dim_str);
extern void heap_init_msmc(char *, size_t);
				

int main(int argc, char *argv [])
{

	//======================================================================================================================================================150
	//	CPU/MCPU VARIABLES
	//======================================================================================================================================================150

	// timer
	double time0 = omp_get_wtime();

	// system memory

	dim_str dim_cpu;


	//======================================================================================================================================================150
	//	CHECK INPUT ARGUMENTS
	//======================================================================================================================================================150

	// assing default values
	dim_cpu.cores_arg = 1;
	dim_cpu.boxes1d_arg = 1;

	// go through arguments
	for(dim_cpu.cur_arg=1; dim_cpu.cur_arg<argc; dim_cpu.cur_arg++){
		// check if -cores
		if(strcmp(argv[dim_cpu.cur_arg], "-cores")==0){
			// check if value provided
			if(argc>=dim_cpu.cur_arg+1){
				// check if value is a number
				if(isInteger(argv[dim_cpu.cur_arg+1])==1){
					dim_cpu.cores_arg = atoi(argv[dim_cpu.cur_arg+1]);
					if(dim_cpu.cores_arg<0){
						printf("ERROR: Wrong value to -cores parameter, cannot be <=0\n");
						return 0;
					}
					dim_cpu.cur_arg = dim_cpu.cur_arg+1;
				}
				// value is not a number
				else{
					printf("ERROR: Value to -cores parameter in not a number\n");
					return 0;
				}
			}
			// value not provided
			else{
				printf("ERROR: Missing value to -cores parameter\n");
				return 0;
			}
		}
		// check if -boxes1d
		else if(strcmp(argv[dim_cpu.cur_arg], "-boxes1d")==0){
			// check if value provided
			if(argc>=dim_cpu.cur_arg+1){
				// check if value is a number
				if(isInteger(argv[dim_cpu.cur_arg+1])==1){
					dim_cpu.boxes1d_arg = atoi(argv[dim_cpu.cur_arg+1]);
					if(dim_cpu.boxes1d_arg<0){
						printf("ERROR: Wrong value to -boxes1d parameter, cannot be <=0\n");
						return 0;
					}
					dim_cpu.cur_arg = dim_cpu.cur_arg+1;
				}
				// value is not a number
				else{
					printf("ERROR: Value to -boxes1d parameter in not a number\n");
					return 0;
				}
			}
			// value not provided
			else{
				printf("ERROR: Missing value to -boxes1d parameter\n");
				return 0;
			}
		}
		// unknown
		else{
			printf("ERROR: Unknown parameter\n");
			return 0;
		}
	}

	// Print configuration
	printf("Launching...\n threads = %d, boxes(1d) = %d\n", dim_cpu.cores_arg, dim_cpu.boxes1d_arg);
	
	printf("MSMC initializing.\n");
	int HeapSize = 4 * (1<<20);//Du
	char* HeapMSMC = (char*) __malloc_msmc(HeapSize);//Du
	heap_init_msmc(HeapMSMC, HeapSize);//Du

	double time1 = omp_get_wtime();

	OffLoad(dim_cpu);
	double time2 = omp_get_wtime();


	//======================================================================================================================================================150
	//	INPUTS
	//======================================================================================================================================================150

	double time3 = omp_get_wtime();


	//======================================================================================================================================================150
	//	DISPLAY TIMING
	//======================================================================================================================================================150

	 printf("\nTime spent in different stages of the application:\n");

	 printf("Main Loop: %f s.\n", time2-time1);
	 printf("Total Time: %f s.\n", time3-time0);

	//======================================================================================================================================================150
	//	RETURN
	//======================================================================================================================================================150

	return 0.0;																					// always returns 0.0

}
