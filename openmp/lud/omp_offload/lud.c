/*
 * =====================================================================================
 *
 *       Filename:  suite.c
 *
 *    Description:  The main wrapper for the suite
 *
 *        Version:  1.0
 *        Created:  10/22/2009 08:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Liang Wang (lw2aw), lw2aw@virginia.edu
 *        Company:  CS@UVa
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <assert.h>
#include <omp.h>

#include "common.h"

static int do_verify = 0;

static struct option long_options[] = {
      /* name, has_arg, flag, val */
      {"input", 1, NULL, 'i'},
      {"size", 1, NULL, 's'},
      {"verify", 0, NULL, 'v'},
      {0,0,0,0}
};

extern float*
lud_omp(float* m, int matrix_dim, int omp_num_threads);


int
main ( int argc, char *argv[] )
{
  int matrix_dim = 32; /* default size */
  int omp_num_threads = 2; // by default
  int opt, option_index=0;
  func_ret_t ret;
	int ii = 0;
  const char *input_file = NULL;
  float* m;
  float* mm;
  stopwatch sw;

	double time0 = omp_get_wtime();

  while ((opt = getopt_long(argc, argv, "::vs:i:n:", 
                            long_options, &option_index)) != -1 ) {
      switch(opt){
        case 'i':
          input_file = optarg;
          break;
		case 'n':
          omp_num_threads = atoi(optarg);
          break;
        case 'v':
          do_verify = 1;
          break;
        case 's':
          matrix_dim = atoi(optarg);
          fprintf(stderr, "Currently not supported, use -i instead\n");
          fprintf(stderr, "Usage: %s [-v] [-n no. of threads] [-s matrix_size|-i input_file]\n", argv[0]);
          exit(EXIT_FAILURE);
        case '?':
          fprintf(stderr, "invalid option\n");
          break;
        case ':':
          fprintf(stderr, "missing argument\n");
          break;
        default:
          fprintf(stderr, "Usage: %s [-v] [-n no. of threads] [-s matrix_size|-i input_file]\n",
                  argv[0]);
          exit(EXIT_FAILURE);
      }
  }
  
  if ( (optind < argc) || (optind == 1)) {
      fprintf(stderr, "Usage: %s [-v] [-n no. of threads] [-s matrix_size|-i input_file]\n", argv[0]);
      exit(EXIT_FAILURE);
  }

  if (input_file) {
      printf("Reading matrix from file %s\n", input_file);
      ret = create_matrix_from_file(&m, input_file, &matrix_dim);
      if (ret != RET_SUCCESS) {
          m = NULL;
          fprintf(stderr, "error create matrix from file %s\n", input_file);
          exit(EXIT_FAILURE);
      }
  } else {
    printf("No input file specified!\n");
    exit(EXIT_FAILURE);
  } 

  if (do_verify){
    printf("Before LUD\n");
    matrix_duplicate(m, &mm, matrix_dim);
}


	double time1 = omp_get_wtime();
	m = lud_omp(m, matrix_dim, omp_num_threads);
	double time2 = omp_get_wtime();

	if (do_verify){
    printf("After LUD\n");
    printf(">>>Verify<<<<\n");	
	lud_verify(mm, m, matrix_dim); 
    free(mm);
  }
	
	free(m);

	double time3 = omp_get_wtime();

	printf("Main Loop: %f s.\n",time2-time1);
	printf("Total Time: %f s.\n",time3-time0);

  return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
